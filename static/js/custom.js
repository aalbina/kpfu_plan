$(document).ready(function () {
    var subjectId = window.location.pathname.split("/").slice(-1).pop();
    (function () {
        "use strict";


        function csrfSafeMethod(method) {
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        }

        function sameOrigin(url) {
            var host = document.location.host, // host + port
                protocol = document.location.protocol,
                sr_origin = '//' + host,
                origin = protocol + sr_origin;
            // Allow absolute or scheme relative URLs to same origin
            return (url == origin || url.slice(0, origin.length + 1) == origin
                + '/')
                || (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin
                + '/') || !(/^(\/\/|http:|https:).*/.test(url));
        }

        $.ajaxSetup({
            beforeSend: function (xhr, settings) {
                if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                    xhr.setRequestHeader("X-CSRFToken", $.cookie('csrftoken'));
                }
            }
        });
    }(this));


    $('.js-nav').on('click', function () {
        var element = $(this).attr("class").slice(-1)[0];
        var selector = '.block-' + element;

        var blocks = $('.block');
        for (var i = 0; i < blocks.length; i++) {
            var block = blocks[i];
            if (!$(block).is(":hidden")) {
                $(block).hide();
            }
        }

        $(selector).slideDown("slow");
    });
    $('.nav-1').on('click', function () {
        $.ajax({
            url: "/min-edu/point/" + subjectId + "/1/",
            type: "GET",
            success: function (data) {
                $('.block-1').html(data);
                $('.p-goals').on('blur', point_one);
            }
        });
    });
    $('.nav-2').on('click', function () {
        $.ajax({
            url: "/min-edu/point/" + subjectId + "/2/",
            type: "GET",
            success: function (data) {
                $('.block-2').html(data);
                $('.posistion-disciplines').on('blur', point_two);
            }
        });
    });
    $('.nav-3').on('click', function () {
        $.ajax({
            url: "/min-edu/point/" + subjectId + "/3/",
            type: "GET",
            success: function (data) {
                $('.block-3').html(data);
                $('#select-1, #select-2, #select-3, #select-4, #select-5, #select-0')
                    .change(point_three_selector);
                $('#textarea-1, #textarea-2, #textarea-3, #textarea-4, #textarea-5, #textarea-0')
                    .on('blur', point_three_textarea);
            }
        });
    });
    $('.nav-4').on('click', function () {
        $.ajax({
            url: "/min-edu/point/" + subjectId + "/4/",
            type: "GET",
            success: function (data) {
                $('.block-4').html(data);
                $('.table-discipline, .table-week-semester, .table-prac-les, .table-self-work, .section-сontents')
                    .on('blur', point_four);
                $('.table-semester, .table-form-control').on('change', point_four);
                $('.js-selfwork-type, .js-hour, .js-control-form').on('blur', point_four_additional);
                $('#add-new-row').on('click', addNewRow);
                $(".additionally-info").on('click', additionallyInfo);
                $('.remove-row').on('click', removeRow);
                $('.new-add-info').on('click', addNewInfo);
                $('.remove-add-info').on('click', removeAddInfo);
            }
        });
    });
    $('.nav-5').on('click', function () {
        $.ajax({
            url: "/min-edu/point/" + subjectId + "/5/",
            type: "GET",
            success: function (data) {
                $('.block-5').html(data);
                $('#edu-tech-textarea').on('blur', point_five);
            }
        });
    });
    $('.nav-6').on('click', function () {
        $.ajax({
            url: "/min-edu/point/" + subjectId + "/6/",
            type: "GET",
            success: function (data) {
                $('.block-6').html(data);
                $('.detail, .js-six-other').on('blur', point_six)
            }
        });
    });
    $('.nav-7').on('click', function () {
        $.ajax({
            url: "/min-edu/point/" + subjectId + "/7/",
            type: "GET",
            success: function (data) {
                $('.block-7').html(data);
                $('textarea[name="main_literature"], textarea[name="additional_literature"]').on('blur', point_seven)
            }
        });
    });
    $('.nav-8').on('click', function () {
        $.ajax({
            url: "/min-edu/point/" + subjectId + "/8/",
            type: "GET",
            success: function (data) {
                $('.block-8').html(data);
                $('input[type="checkbox"], .js-eight-other').on('change', point_eight)
            }
        });
    });


});
function removeAddInfo() {
    var subjectId = window.location.pathname.split("/").slice(-1).pop();
    var infos = $(this).closest('.js-additional-row').find('.add-info');
    var infoSpecId = $(this).closest('.js-additional-row').find('.js-add-info-id').val();
    var specId = $(this).closest('.add-info').find('.js-add-info-id').val();
    var $remove = $(this);
    if (infos.length > 1) {
        $.ajax({
            url: "/min-edu/point/" + subjectId + "/4/",
            type: "DELETE",
            data: {
                specId: specId,
                infoSpecId: infoSpecId
            },
            success: function (data) {
                var id = $remove.closest('.add-info').children('.js-add-info-id').val();
                $remove.closest('.add-info').remove();
                $('.new-add-info:last').show();
            }
        });
    } else {
        alert("Нельзя удалить последний элемент");
    }
}

function addNewRow() {

    var jsMainRow = $('.js-main-row:last');
    var jsAddRow = $('.js-additional-row:last');


    $(jsMainRow).clone(true).insertAfter(jsAddRow);
    $(jsAddRow).clone(true).insertAfter($('tr.js-main-row').last());

    var rowNum = $('.table-num:last');
    rowNum.val(parseInt(rowNum.val()) + 1);

    $('.js-additional-row:last').find(".add-info:nth-child(3)").nextAll().remove();

    clear_form_elements('js-main-row:last', 'table-num');
    clear_form_elements('js-additional-row:last');

    var additionalId = $('.js-add-row-id:last');
    additionalId.val(parseInt(additionalId.val()) + 1);
    var mainId = $('.js-main-row-id:last');
    mainId.val(parseInt(mainId.val()) + 1);


}
function addNewInfo() {
    $(this).closest('.add-info').clone(true).insertAfter('.add-info:last');
    $(this).hide();
    clear_form_elements('add-info:last');
    var addInfoId = $('.js-add-info-id:last');
    addInfoId.val(parseInt(addInfoId.val()) + 1);

}
function additionallyInfo() {
    var info = $(this).parent().parent().next();
    if (info.is(":hidden")) {
        info.slideDown("slow");
    } else {
        info.slideUp();
    }
}

function clear_form_elements(class_name, without) {
    jQuery("." + class_name).find(':input').each(function () {
        if ($.inArray(without, $(this).attr('class').split(" "))) {
            switch (this.type) {
                case 'password':
                case 'text':
                case 'textarea':
                case 'file':
                case 'number':
                case 'select-one':
                case 'select-multiple':
                    jQuery(this).val('');
                    break;
                case 'checkbox':
                case 'radio':
                    this.checked = false;
            }
        }
    });
}
function removeRow() {
    var rows = $('.remove-row').closest("tbody").find('.js-main-row');
    if (rows.length > 1) {
        $(this).closest(".js-main-row").next('.js-additional-row').remove();
        $(this).closest(".js-main-row").remove();
    } else {
        alert("Нельзя удалить последний элемент");
    }
}
$(document).ready(function () {
    var subjectId = window.location.pathname.split("/").slice(-1).pop();
    (function () {
        "use strict";

        function csrfSafeMethod(method) {
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        }

        function sameOrigin(url) {
            var host = document.location.host, // host + port
                protocol = document.location.protocol,
                sr_origin = '//' + host,
                origin = protocol + sr_origin;
            // Allow absolute or scheme relative URLs to same origin
            return (url == origin || url.slice(0, origin.length + 1) == origin
                + '/')
                || (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin
                + '/') || !(/^(\/\/|http:|https:).*/.test(url));
        }

        $.ajaxSetup({
            beforeSend: function (xhr, settings) {
                if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                    xhr.setRequestHeader("X-CSRFToken", $.cookie('csrftoken'));
                }
            }
        });
    }(this));

    /**
     *point 1
     */
    $('.p-goals').on('blur', point_one);
    $('.p-goals-doc').on('change', point_one);

    /**
     * point 2
     */
    $('.posistion-disciplines').on('blur', point_two);

    /**
     * point 3
     */

    $('#select-1, #select-2, #select-3, #select-4, #select-5, #select-0').change(point_three_selector);
    $('#textarea-1, #textarea-2, #textarea-3, #textarea-4, #textarea-5, #textarea-0').on('blur', point_three_textarea);

    /**
     * point 4
     */
    $('.table-discipline, .table-week-semester, .table-prac-les, .table-self-work, .section-сontents')
        .on('blur', point_four);

    $('.table-semester, .table-form-control').on('change', point_four);

    $('.js-selfwork-type, .js-hour, .js-control-form').on('blur', point_four_additional);
    /**
     * point 5
     */
    $('#edu-tech-textarea').on('blur', point_five);

    /**
     * point 6
     */
    $('.detail, .js-six-other').on('blur', point_six);
    /**
     * point 7
     */
    $('textarea[name="main_literature"], textarea[name="additional_literature"]').on('blur', point_seven);
    /**
     * point 8
     */
    $('input[type="checkbox"]').on('change', point_eight);
});

function point_five() {
    var subjectId = window.location.pathname.split("/").slice(-1).pop();
    var message = $(this).val();
    if (message.length != 0 || message.replace(/\s/g, "").length != 0) {
        $.ajax({
            url: "/min-edu/point/" + subjectId + "/5/",
            type: "POST",
            data: {
                value: message
            },
            success: function (data) {
                console.log("select " + $(this).attr("id") + " was send")
            }
        })
    }
}
function point_four() {
    var subjectId = window.location.pathname.split("/").slice(-1).pop();
    var message = $(this).val();
    var specId = $(this).closest('.js-main-row').find('.js-main-row-id').val();
    var columnName = $(this).attr('name');
    if (message.length != 0 || message.replace(/\s/g, "").length != 0) {
        $.ajax({
            url: "/min-edu/point/" + subjectId + "/4/",
            type: "POST",
            data: {
                value: message,
                specId: specId,
                columnName: columnName
            },
            success: function (data) {
                console.log("select " + $(this).attr("name") + " was send")
            }
        });
    }
}

function point_six() {
    var message = $(this).val();
    var subjectId = window.location.pathname.split("/").slice(-1).pop();
    if (message.length != 0 || message.replace(/\s/g, "").length != 0) {
        var idAndType = $(this).prev().val().split("-");
        var isOther = $.inArray('js-six-other', $(this).attr('class').split(" ")) >= 0;
        var data = (isOther ?
        {
            value: message,
            type: 'other'
        }
            :
        {
            value: message,
            fconId: idAndType[0],
            type: idAndType[1]
        });

        $.ajax({
            url: "/min-edu/point/" + subjectId + "/6/",
            type: "POST",
            data: data,
            success: function (data) {
                console.log("select " + $(this).attr("id") + " was send")
            }
        })
    }
}
function point_seven() {
    var subjectId = window.location.pathname.split("/").slice(-1).pop();
    var message = $(this).val();
    if (message.length != 0 || message.replace(/\s/g, "").length != 0) {
        $.ajax({
            url: "/min-edu/point/" + subjectId + "/7/",
            type: "POST",
            data: {
                value: message,
                type: $(this).attr('name')
            },
            success: function (data) {
                console.log("select " + $(this).attr("name") + " was send")
            }
        });
    }
}

function point_eight() {
    var subjectId = window.location.pathname.split("/").slice(-1).pop();
    var message = $(this).is(':checked');
    if (message.length != 0 || message.replace(/\s/g, "").length != 0) {
        if ($(this).attr('type') == 'checkbox') {
            isOther = false;
        } else {
            var isOther = $.inArray('js-eight-other', $(this).attr('class').split(" ")) >= 0;
        }
        var data = (isOther ?
        {
            value: $(this).val(),
            type: 'other'
        }
            :
        {
            value: message,
            spec_id: $(this).val()
        });
        $.ajax({
            url: "/min-edu/point/" + subjectId + "/8/",
            type: "POST",
            data: data,
            success: function (data) {
                console.log("select " + $(this).attr("name") + " was send")
            }
        });
    }
}

function point_one() {
    var subjectId = window.location.pathname.split("/").slice(-1).pop();

    var formData = new FormData();
    formData.append('file', $('.p-goals-doc')[0].files[0]);
    var message_text = $('.p-goals').val();
    if (message_text.length != 0 || message_text.replace(/\s/g, "").length != 0) {
        formData.append('p_goal', message_text);

        $.ajax({
            url: "/min-edu/point/" + subjectId + "/1/",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
                console.log("ok");
            }
        });
    }
}

function point_two() {
    var subjectId = window.location.pathname.split("/").slice(-1).pop();
    var message = $('.posistion-disciplines').val();
    if (message.length != 0 || message.replace(/\s/g, "").length != 0) {


        $.ajax({
            url: "/min-edu/point/" + subjectId + "/2/",
            type: "POST",
            data: {
                posistionDisciplines: message
            },
            success: function (data) {
                console.log("ok");
            }
        });
    }
}

function point_three_selector() {
    var subjectId = window.location.pathname.split("/").slice(-1).pop();
    var message = $(this).val();
    if (message.length != 0 || message.replace(/\s/g, "").length != 0) {
        $.ajax({
            url: "/min-edu/point/" + subjectId + "/3/",
            type: "POST",
            data: {
                type: "select",
                spec_id: $(this).attr("id").split("-")[1],
                value: message
            },
            success: function (data) {
                console.log("select " + $(this).attr("id") + " was send")
            }
        })
    }
}
function point_three_textarea() {
    var subjectId = window.location.pathname.split("/").slice(-1).pop();
    var message = $(this).val();
    if (message.length != 0 || message.replace(/\s/g, "").length != 0) {
        $.ajax({
            url: "/min-edu/point/" + subjectId + "/3/",
            type: "POST",
            data: {
                type: "text",
                spec_id: $(this).attr("id").split("-")[1],
                value: message
            },
            success: function (data) {
                console.log("select " + $(this).attr("id") + " was send")
            }
        })
    }
}
function point_four_additional() {
    var subjectId = window.location.pathname.split("/").slice(-1).pop();
    var message = $(this).val();
    var specId = $(this).closest('.add-info').find('.js-add-info-id').val();
    var columnName = $(this).attr('name');
    var infoSpecId = $(this).closest('.js-additional-row').find('.js-add-info-id').val();
    if (message.length != 0 || message.replace(/\s/g, "").length != 0) {
        $.ajax({
            url: "/min-edu/point/" + subjectId + "/4/",
            type: "POST",
            data: {
                value: message,
                specId: specId,
                columnName: columnName,
                infoSpecId: infoSpecId

            },
            success: function (data) {
                console.log("select " + $(this).attr("name") + " was send")
            }

        });
    }
}
from django.contrib import admin

# Register your models here.
from min_obr.models import CompetenceTrainees, Description, InternetResource, \
    MinistryOfEdu, SelfTime, StructureAndContent, Support

admin.site.register(CompetenceTrainees)
admin.site.register(Description)
admin.site.register(InternetResource)
admin.site.register(MinistryOfEdu)
admin.site.register(SelfTime)
admin.site.register(StructureAndContent)
admin.site.register(Support)

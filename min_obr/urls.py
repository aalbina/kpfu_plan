from django.conf.urls import url

urlpatterns = [
    url(r'^(?P<subject_id>[0-9]+)$', 'min_obr.views.new', name='new'),


    # points ajax request
    url(r'^point/(?P<subject_id>[0-9]+)/(?P<point_id>[0-9]+)/$', 'min_obr.views.point_service', name='point-service'),
]

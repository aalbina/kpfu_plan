# coding=utf-8
from django.contrib.auth.models import User
from django.db import models
import os

def get_goal_doc_url(instance, filename):
    return os.path.join('goal_docs', filename)


class CompetenceTrainees(models.Model):
    cipher = models.CharField(max_length=50)
    decryption = models.CharField(max_length=255)
    edu = models.ForeignKey('MinistryOfEdu', db_column='edu')
    additional = models.BooleanField()
    spec_id = models.IntegerField(null=True)

    class Meta:
        db_table = 'competence_trainees'


class Description(models.Model):
    author = models.TextField(blank=True, null=True)
    external_author = models.BooleanField()
    reviewers = models.TextField(blank=True, null=True)
    external_reviewers = models.BooleanField()
    min_edu = models.ForeignKey('MinistryOfEdu')

    class Meta:
        db_table = 'description'


class InternetResource(models.Model):
    name = models.CharField(max_length=255)
    link = models.CharField(max_length=255)
    min_edu = models.ForeignKey('MinistryOfEdu')

    class Meta:
        db_table = 'internet_resource'


class MinistryOfEdu(models.Model):
    goals_disciplines = models.TextField(blank=True, null=True)
    goals_document = models.FileField(blank=True, upload_to=get_goal_doc_url)
    posistion_disciplines = models.TextField(blank=True, null=True)
    edu_technology = models.TextField()
    # TODO: devide main and additional literature for capability to add docs
    main_literature = models.TextField(blank=True, null=True, max_length=255)
    additional_literature = models.TextField(blank=True, null=True, max_length=255)
    six_other = models.TextField(blank=True, null=True, max_length=255)
    eight_other = models.TextField(blank=True, null=True, max_length=255)

    class Meta:
        db_table = 'ministry_of_edu'


class StructureAndContent(models.Model):
    discipline = models.TextField(null=True)
    semester = models.IntegerField(null=True)
    week_of_semester = models.CharField(max_length=10, null=True)
    lecture = models.IntegerField(blank=True, null=True)
    practice = models.IntegerField(blank=True, null=True)
    labs = models.IntegerField(blank=True, null=True)
    self_work = models.IntegerField(null=True)
    control_type = models.CharField(max_length=2, null=True)
    content = models.TextField(blank=True, null=True)
    monitoring_progress = models.TextField(null=True)
    min_edu = models.ForeignKey(MinistryOfEdu)
    spec_id = models.IntegerField(null=True)
    test = models.CharField(null=True, max_length=255)
    homework = models.CharField(null=True, max_length=255)

    class Meta:
        db_table = 'structure_and_content'


class SelfTime(models.Model):
    type_self_work = models.TextField(null=True)
    time = models.IntegerField(null=True)
    control_form = models.TextField(null=True)
    structure = models.ForeignKey(StructureAndContent, null=True)
    spec_id = models.IntegerField(null=True)

    class Meta:
        db_table = 'self_time'


class Support(models.Model):
    is_enable = models.BooleanField(default=False)
    min_edu = models.ForeignKey(MinistryOfEdu)
    spec_id = models.IntegerField(null=True)

    class Meta:
        db_table = 'support'


class UserAndSubject(models.Model):
    user = models.ForeignKey(User, null=False)
    minEdu = models.ForeignKey(MinistryOfEdu, null=True)
    subject = models.CharField(max_length=255, null=True)

    class Meta:
        db_table = 'user_and_subject'

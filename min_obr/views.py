# coding=utf-8
from django.contrib.auth.decorators import login_required
from django.db.models import QuerySet
from django.http import HttpResponse
from django.shortcuts import render, render_to_response

# Create your views here.
from django.template import RequestContext

from min_obr.models import MinistryOfEdu, UserAndSubject, CompetenceTrainees, StructureAndContent, SelfTime, Support


@login_required(login_url="/login")
def new(request, subject_id):
    return render(request, 'min_obr/index.html', {})


def point_service(request, subject_id, point_id):
    points = {
        0: zero,
        1: one,
        2: two,
        3: three,
        4: four,
        5: five,
        6: six,
        7: seven,
        8: eight,
        # add more
    }
    subject = UserAndSubject.objects.get(id=subject_id)
    if subject.minEdu is None:
        minister_of_edu = MinistryOfEdu()
        minister_of_edu.save()
        subject.minEdu = minister_of_edu
        subject.save()
        response = points[int(point_id)](request, subject=subject, min_edu=minister_of_edu)
    else:
        minister_of_edu = MinistryOfEdu.objects.get(id=subject.minEdu.id)
        response = points[int(point_id)](request, subject=subject, min_edu=minister_of_edu)
    return response if type(response) == HttpResponse else HttpResponse(status=200)


def zero(request):
    pass


def one(request, min_edu, subject):
    context = RequestContext(request)
    if request.method == 'POST':
        try:
            min_edu.goals_disciplines = request.POST.get('p_goal')
            min_edu.save()
        except KeyError:
            pass

        try:
            min_edu.goals_document = request.FILES.get('file')
            min_edu.save()
        except KeyError:
            pass
    else:
        return (render_to_response("min_obr/forms/point-1.html",
                                   {'minEdu': min_edu},
                                   context
                                   ))


def two(request, min_edu, subject):
    context = RequestContext(request)
    if request.method == 'POST':
        min_edu.posistion_disciplines = request.POST.get('posistionDisciplines')
        min_edu.save()
    else:
        return (render_to_response("min_obr/forms/point-2.html",
                                   {'minEdu': min_edu},
                                   context
                                   ))


def three(request, min_edu, subject):
    context = RequestContext(request)
    if request.method == "POST":
        competence_trainees = CompetenceTrainees.objects.filter(edu=min_edu.id,
                                                                spec_id=int(request.POST.get("spec_id")))
        additional = False if int(request.POST.get('spec_id')) != 0 else True
        if len(competence_trainees) == 0:
            competence_trainees = CompetenceTrainees(additional=additional,
                                                     edu=subject.minEdu,
                                                     spec_id=request.POST.get('spec_id'))
            if request.POST.get("type") == "select":
                competence_trainees.cipher = request.POST.get('value')
            else:
                competence_trainees.decryption = request.POST.get("value")
            competence_trainees.save()
        else:
            for i in competence_trainees:
                if i.spec_id == int(request.POST.get('spec_id')):
                    if request.POST.get("type") == "select":
                        i.cipher = request.POST.get('value')
                    else:
                        i.decryption = request.POST.get("value")
                    i.save()
    else:
        competence_trainees = CompetenceTrainees.objects.filter(edu=min_edu.id)
        return (render_to_response("min_obr/forms/point-3.html",
                                   {
                                       'minEdu': min_edu,
                                       'competence': competence_trainees
                                   },
                                   context))


def four(request, min_edu, subject):
    context = RequestContext(request)
    if request.method == 'POST':
        structure_and_content = StructureAndContent.objects.filter(min_edu=min_edu,
                                                                   spec_id=int(request.POST.get('specId')))
        if len(structure_and_content) == 0:
            structure_and_content = StructureAndContent(min_edu=min_edu)
            structure_and_content.spec_id = request.POST.get('specId')

        if type(structure_and_content) == QuerySet:
            structure_and_content = structure_and_content.first()
        if 'infoSpecId' not in request.POST:
            setattr(structure_and_content, request.POST.get('columnName'), request.POST.get('value'))
            structure_and_content.save()
        else:
            self_time = SelfTime.objects.filter(structure=structure_and_content,
                                                spec_id=int(request.POST.get('specId')))
            if len(self_time) == 0:
                self_time = SelfTime(structure=structure_and_content,
                                     spec_id=int(request.POST.get('specId')))
            if type(self_time) == QuerySet:
                self_time = self_time.first()
            setattr(self_time, request.POST.get('columnName'), request.POST.get('value'))
            self_time.save()
    elif request.method == 'DELETE':
        structure_and_content = StructureAndContent.objects.get(min_edu=min_edu,
                                                                spec_id=int(request.DELETE.get('specId')))
        if structure_and_content is not None:
            if request.DELETE.get('infoSpecId') is not None:
                try:
                    self_time = SelfTime.objects.get(structure=structure_and_content,
                                                     spec_id=int(request.DELETE.get('specId')))
                except SelfTime.DoesNotExist:
                    self_time = None
                if self_time is not None:
                    self_time.delete()
            else:
                structure_and_content.delete()
    elif request.method == 'GET':
        structure_and_content = StructureAndContent.objects.filter(min_edu=min_edu)
        return (render_to_response("min_obr/forms/point-4.html",
                                   {'structure_and_content': structure_and_content},
                                   context
                                   ))


def five(request, min_edu, subject):
    context = RequestContext(request)
    if request.method == 'POST':
        min_edu.edu_technology = request.POST.get("value")
        min_edu.save()
    else:
        return (render_to_response("min_obr/forms/point-5.html",
                                   {'minEdu': min_edu},
                                   context
                                   ))


def six(request, min_edu, subject):
    context = RequestContext(request)
    structure_and_content = StructureAndContent.objects.filter(min_edu=min_edu, control_type__isnull=False)
    if request.method == 'POST':
        if request.POST.get('type') == 'other':
            min_edu.six_other = request.POST.get('value')
            min_edu.save()
        else:
            structure = StructureAndContent.objects.get(id=request.POST.get('fconId'))
            if request.POST.get('type') == 'h':
                structure.homework = request.POST.get('value')
            else:
                structure.test = request.POST.get('value')
            structure.save()

    else:
        return (render_to_response("min_obr/forms/point-6.html",
                                   {'rows': structure_and_content,
                                    'minEdu': min_edu},
                                   context
                                   ))


def seven(request, min_edu, subject):
    context = RequestContext(request)
    if request.method == 'POST':
        setattr(min_edu, request.POST.get('type'), request.POST.get('value'))
        min_edu.save()
    else:
        return (render_to_response("min_obr/forms/point-7.html",
                                   {'minEdu': min_edu},
                                   context
                                   ))


def eight(request, min_edu, subject):
    context = RequestContext(request)
    if request.method == 'POST':
        if request.POST.get('type') == 'other':
            min_edu.eight_other = request.POST.get('value')
            min_edu.save()
        else:
            try:
                support = Support.objects.get(min_edu=min_edu, spec_id=request.POST.get('spec_id'))
            except Support.DoesNotExist:
                support = None
            if support is None:
                support = Support(min_edu=min_edu,
                                  spec_id=request.POST.get('spec_id'),
                                  is_enable=request.POST.get('value'))
                support.save()
            else:
                if request.POST.get('value') == 'false':
                    support.delete()

    else:
        supports = Support.objects.filter(min_edu=min_edu)
        return (render_to_response("min_obr/forms/point-8.html",
                                   {'minEdu': min_edu,
                                    'supports': supports},
                                   context
                                   ))

from django import template

register = template.Library()


@register.filter
def filter_by_id(list, args):
    args = args.split(' ')
    id = args[0]
    returning = args[1]
    for i in list:
        if i.spec_id == int(id):
            return returning


@register.simple_tag
def find_selected(list, position_id, cipher, returning):
    if len(list) == 0 and int(position_id) == -1:
        return returning
    for i in list:
        if i.spec_id == int(position_id) and int(i.cipher) == int(cipher):
            return returning


@register.simple_tag
def get_decryption(list, position_id):
    if len(list) == 0:
        return ''
    for i in list:
        if i.spec_id == int(position_id):
            return i.decryption if i.decryption is not None else ''
    return ''


@register.simple_tag
def get_selection_by_id(actual_field, id):
    if (int(actual_field) == int(id)) or (actual_field is None and int(id) == -1):
        return 'selected'

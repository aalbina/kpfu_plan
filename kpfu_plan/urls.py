from django.conf.urls import url

urlpatterns = [
    url(r'^$', 'kpfu_plan.views.subject', name="subj_new"),
    url(r'^(?P<subject_id>[0-9]+)$', 'kpfu_plan.views.present_view', name='present')
]

# Create your views here.
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login, logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse, reverse_lazy
from django.db.models import Sum
from django.http import HttpResponseRedirect as redirect, Http404
from django.shortcuts import render

from kpfu_plan.forms import LoginForm
from min_obr.models import UserAndSubject, MinistryOfEdu, CompetenceTrainees, StructureAndContent, InternetResource


def login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            user = authenticate(
                username=form.cleaned_data["login"],
                password=form.cleaned_data["password"]
            )
            if user is not None:
                auth_login(request, user)

                if "next" in request.GET:
                    redir = redirect(request.GET["next"])
                else:
                    redir = redirect(reverse("subj_new"))
                redir.set_cookie("who", user.username, max_age=24 * 60 * 60)
                return redir
            else:
                return render(request, "login.html", {"form": form,
                                                      "errors": ["User is not found."]})
        else:
            return render(request, "login.html", {"form": form})
    else:

        form = LoginForm()
        if "who" in request.COOKIES:
            # form = LoginForm(initial={"username": request.COOKIES["who"]})
            # form.fields["login"].initial = request.COOKIES["who"]
            form.fields["login"].initial = request.COOKIES["who"]
        return render(request, "login.html", {"form": form})


@login_required(login_url="/login")
def logout(request):
    auth_logout(request)
    return redirect("/login")


@login_required(login_url="/login")
def subject(request):
    if request.method == "POST":
        user_and_subject = UserAndSubject(user=request.user,
                                          subject=request.POST["subject"])
        user_and_subject.save()
        return redirect(reverse_lazy("new", kwargs={"subject_id": user_and_subject.id}))
    else:
        subjects = UserAndSubject.objects.filter(user=request.user)
        return render(request, 'kpfu_plan/createSubject.html', {"subjects": subjects})


@login_required(login_url="/login")
def present_view(request, subject_id):
    try:
        subject = UserAndSubject.objects.get(id=subject_id)
        min_edu = MinistryOfEdu.objects.get(id=subject.minEdu_id)
        competences = CompetenceTrainees.objects.filter(edu=min_edu, additional=False).order_by('id')
        additional_competences = CompetenceTrainees.objects.filter(edu=min_edu, additional=True).order_by('id')
        themes = StructureAndContent.objects.filter(min_edu=min_edu).order_by('id')

        internet_resources = InternetResource.objects.filter(min_edu=min_edu)
        return render(request, 'kpfu_plan/index.html',
                      {
                          "subject": subject,
                          "min_edu": min_edu,
                          "competences": competences,
                          "additional_competences": additional_competences,
                          "internet_resources": internet_resources,
                          "themes": themes
                      })

    except ObjectDoesNotExist:
        raise Http404

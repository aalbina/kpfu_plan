from django import forms


class LoginForm(forms.Form):
    login = forms.CharField(label="USERNAME")
    password = forms.CharField(widget=forms.PasswordInput)
